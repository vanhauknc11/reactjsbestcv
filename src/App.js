import React, { Component } from 'react';
import { Route, Switch, BrowserRouter as Router } from 'react-router-dom';
import './app.scss';
import Menu from './components/Menu/Menu';
import LoginForm from './components/LoginForm/index';
import routes from './routes';
import callApi from './utils/apiCaller';
import swal from 'sweetalert';
const sleep = (milliseconds) => {
  return new Promise(resolve => setTimeout(resolve, milliseconds))
}
class App extends Component {


  onSubmit = (data) => {
    console.log(data);
    var txtEmail = data.txtEmail;
    var txtPass = data.txtPass;
    callApi('login', 'POST', {
      email: txtEmail,
      password: txtPass
    }).then(res => {
      var data = res.data;
      console.log(data);
      if (data) {
        if (data.data) {
          if (data.data.idRole.name === "admin") {
            swal("Thông Báo !", "Đăng nhập thành công !", "success");
            localStorage.setItem('user', JSON.stringify({
              id: data.data._id,
            }));
            sleep(3000).then(() => {
              window.location.reload();
            })
          }
        } else {
          swal("Thông Báo !", data.message, "error");
        }
      }


    });


  }



  checkLogin = () => {
    var result = "";
    var isLogin = localStorage.getItem('user');
    if (!isLogin) {
      result = <Router>
        <div>
          <LoginForm onSubmit={this.onSubmit} />
        </div>
      </Router>
    } else {
      result = <Router>
        <div>
          <Menu />
          <div className="container-fluid">
            <div className="row">
              {this.showContentMenus(routes)}
            </div>
          </div>
        </div>
      </Router>
    }
    return result;
  }
  render() {

    return (
      this.checkLogin()
    );
  }

  showContentMenus = (routes) => {
    var result = null;
    if (routes.length > 0) {
      result = routes.map((route, index) => {
        return (
          <Route
            key={index}
            path={route.path}
            exact={route.exact}
            component={route.main}
          />
        );
      });
    }
    return <Switch>
      {result}
    </Switch>
  }

}

export default App;