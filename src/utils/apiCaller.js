import axios from 'axios';
import * as Config from './../constants/Config';
import Stringify from 'query-string';

export default function callApi(endpoint, method, body) {
    return axios({
        method: method,
        url: `${Config.API_URL}/${endpoint}`,
        headers: {
            'content-type': 'application/x-www-form-urlencoded',
        },
        data: Stringify.stringify(body)
    }).catch(err => {
        console.log(err);
    });
}