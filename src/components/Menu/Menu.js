import React, { Component } from 'react';
import { Route, Link } from 'react-router-dom';
const menus = [
    {
        name: 'Quản Lý Bài Viết',
        to: '/',
        exact: true
    },
    {
        name: 'Quản Lý Sản Phẩm',
        to: '/product-list',
        exact: false
    },
    {
        name: 'Quản Lý Giao Dịch',
        to: '/transaction',
        exact: false
    },
    {
        name: 'Quản Lý Tài Khoản',
        to: '/usermanage',
        exact: false
    },


]
const MenuLink = ({ label, to, activeOnlyWhenExact }) => {
    return (
        <Route
            path={to}
            exact={activeOnlyWhenExact}
            children={({ match }) => {
                var active = match ? 'active' : '';
                return (
                    <li className={active}>
                        <Link to={to}>
                            {label}
                        </Link>
                    </li>
                );
            }}
        />
    );
}



class Menu extends Component {
    onLogout = () => {
        localStorage.removeItem('user');
        window.location.reload();
    }
    render() {
        return (
            <div className="navbar navbar-default " >
                <a href="/#" className="navbar-brand" >BESTCV ADMIN</a>
                <ul className="nav navbar-nav row">
                    {this.showMenus(menus)}

                </ul>

                <ul className="nav navbar-nav navbar-right">
                    <li><button type="button" className="btn btn-danger navbar-btn" style={{ marginRight: '30px' }} onClick={this.onLogout}>Log Out</button></li>

                </ul>

            </div>



        );
    }
    showMenus = (menus) => {
        var result = null;
        if (menus.length > 0) {
            result = menus.map((menu, index) => {
                return (
                    <MenuLink
                        key={index}
                        label={menu.name}
                        to={menu.to}
                        activeOnlyWhenExact={menu.exact}
                    />
                );
            });
        }

        return result;
    }

}

export default Menu;